station_cfg={}
station_cfg.ssid  = "WIFINAME"       -- Bağlanacağınız Wi-Fi ağının adı.
station_cfg.pwd   = "PASSWORD"  -- Bağlanacağınız Wi-Fi ağının şifresi.
station_cfg.save  = true

counter      = 0
wifiTrys     = 15                -- Wifi'a bağlanmayı denemek için gerekli olan counter.
NUMWIFITRYS  = 50                -- Wifi'a bağlanmak için gerekli olan maksimum deneme sayısı.

tgtHost     = "192.168.1.121"    -- Broker'ın IP'si.
tgtPort     = 1883               -- Broker'ın Port Numarası.
mqttTimeOut = 120                -- Bağlantının kopması için gerekli olan süre(saniye cinsinden).
dataInt     = 2                  -- Gönderim yapılacak sıklık süresi(2 saniyede bir).
topicQueue  = "test"             -- Bağlanılacak MQTT topiği.

ipAddr = wifi.sta.getip()

if ( ( ipAddr == nil ) or  ( ipAddr == "0.0.0.0" ) ) then
    -- Şuan bağlı değiliz, bağlanmayı deneyelim.
    wifi.setmode( wifi.STATION )
    wifi.sta.config(station_cfg)
    tmr.alarm( 0 , 2500 , 0 , checkWIFI )
else
    -- Wifi'a bağlıyız, diğer işlemleri yapacağımız fonksiyona geçelim.
    makeConn()
end

function checkWIFI() 
    
    if ( wifiTrys > NUMWIFITRYS ) then
    
    else
    --Eğer maksimum deneme sayısına ulaşılmadıysa.
    ipAddr = wifi.sta.getip()
   
    if ( ( ipAddr ~= nil ) and  ( ipAddr ~= "0.0.0.0" ) )then
      --Eğer modül şuan bir ağa bağlı değilse.
      tmr.alarm( 1 , 500 , 0 , makeConn ) 
    else
      -- Wifi'a bağlanmayı tekrar dene ve counter'ı arttır.
      tmr.alarm( 0 , 2500 , 0 , checkWIFI)
      wifiTrys = wifiTrys + 1
    end 
  end 
end

function pubEvent()
    rv = "Votec45"                                 
    counter=counter+1    
    pubValue = rv .. " " .. counter                 -- Göndereceğimiz mesajımızı(verimizi) şekillendirdiğimiz satır.   
    mqttBroker:publish(topicQueue, pubValue, 1, 0)  -- Topic ve göndereceğimiz mesaj parametrelerini alan publish fonksiyonumuz.    
end
 
-- Offline mesajını aldığımızda tekrar bağlanmayı deneyeceğimiz fonksiyon.
function reconn()
    conn()
end
  
-- MQTT Broker'a gerekli olan parametreler aracılığıyla bağlanma.
function conn()
    mqttBroker:connect(tgtHost, tgtPort, 0, function(client) end, function(client, reason) end)
end
 
function makeConn()
    -- Global MQTT Client objesi yaratıyoruz.
    mqttBroker = mqtt.Client(votec45, mqttTimeOut)
 
    -- Geri bildirimleri değerlendirip, ona göre hareket ettiğimiz kısım.
    mqttBroker:on("connect", function(client) end)
    mqttBroker:on("offline", reconn)
 
    -- Broker'a bağlan.
    conn()
 
    -- Veriyi MQTT Broker'da gerekli topic'e yolladığımız fonksiyon.
    tmr.alarm(0, (dataInt * 1000), 1, pubEvent)
end
